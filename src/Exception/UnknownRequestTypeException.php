<?php
/**
*	This file contains the Unknown Request Type Exception Class.
*
*	@package	Accommodationuk\RightmoveADF
*	@author		Jacob Wyke <jacob@frozensheep.com>
*	@license	MIT
*
*/

namespace Accommodationuk\RightmoveADF\Exception;

/**
*	Unknown Request Type Exception Class
*
*	Exception for an unknown request type.
*
*	@package	Accommodationuk\RightmoveADF
*
*/
class UnknownRequestTypeException extends \RuntimeException {

	/**
	*	Constructor
	*
	*	@return self
	*/
	public function __construct(){
		parent::__construct('Unknown Request Type.');
	}
}
<?php
/**
*	This file contains the Furnishings Enum class.
*
*	@package	Accommodationuk\RightmoveADF
*	@author		Jacob Wyke <jacob@frozensheep.com>
*	@license	MIT
*
*/

namespace Accommodationuk\RightmoveADF\Values;

use Accommodationuk\RightmoveADF\Values\ValuesBase;

/**
*	Furnishings Enum Class
*
*	Class for the different Furnishing options.
*
*	@package	Accommodationuk\RightmoveADF
*
*/
class Furnishings extends ValuesBase {

	const Furnished = 0;
	const PartFurnished = 1;
	const Unfurnished = 2;
	const FurnishedUnfurnished = 4;
}
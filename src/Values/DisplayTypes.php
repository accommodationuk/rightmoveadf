<?php
/**
*	This file contains the Display Types Enum class.
*
*	@package	Accommodationuk\RightmoveADF
*	@author		Jacob Wyke <jacob@frozensheep.com>
*	@license	MIT
*
*/

namespace Accommodationuk\RightmoveADF\Values;

use Accommodationuk\RightmoveADF\Values\ValuesBase;

/**
*	Display Types Enum Class
*
*	Class for the different Display Types.
*
*	@package	Accommodationuk\RightmoveADF
*
*/
class DisplayTypes extends ValuesBase {

	const OneLargeTwoSmall = 1;
	const OneLargeImage = 2;
}
<?php
/**
*	This file contains the Featured Property Types Enum class.
*
*	@package	Accommodationuk\RightmoveADF
*	@author		Jacob Wyke <jacob@frozensheep.com>
*	@license	MIT
*
*/

namespace Accommodationuk\RightmoveADF\Values;

use Accommodationuk\RightmoveADF\Values\ValuesBase;

/**
*	Featured Property Types Enum Class
*
*	Class for the different Featured Property Types.
*
*	@package	Accommodationuk\RightmoveADF
*
*/
class FeaturedPropertyTypes extends ValuesBase {

	const Subscription = 1;
	const FeaturedPropertyOfTheWeek = 2;
}
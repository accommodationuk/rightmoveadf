<?php
/**
*	This file contains the Commercial Use Classes Enum class.
*
*	@package	Accommodationuk\RightmoveADF
*	@author		Jacob Wyke <jacob@frozensheep.com>
*	@license	MIT
*
*/

namespace Accommodationuk\RightmoveADF\Values;

use Accommodationuk\RightmoveADF\Values\ValuesBase;

/**
*	Commercial Use Classes Enum Class
*
*	Class for the different commercial use classes.
*
*	@package	Accommodationuk\RightmoveADF
*
*/
class CommercialUseClasses extends ValuesBase {

	const A1 = 1;
	const A2 = 4;
	const A3 = 7;
	const A4 = 10;
	const A5 = 13;
	const B1 = 16;
	const B2 = 19;
	const B8 = 22;
	const C1 = 25;
	const C2 = 28;
	const C2A = 31;
	const C3 = 34;
	const D1 = 37;
	const D2 = 40;
	const Sui_generis_1 = 43;
	const Sui_generis_2 = 46;
}
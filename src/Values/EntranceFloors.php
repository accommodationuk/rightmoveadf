<?php
/**
*	This file contains the Entrance Floors Enum class.
*
*	@package	Accommodationuk\RightmoveADF
*	@author		Jacob Wyke <jacob@frozensheep.com>
*	@license	MIT
*
*/

namespace Accommodationuk\RightmoveADF\Values;

use Accommodationuk\RightmoveADF\Values\ValuesBase;

/**
*	Entrance Floors Enum Class
*
*	Class for the different entrance floors.
*
*	@package	Accommodationuk\RightmoveADF
*
*/
class EntranceFloors extends ValuesBase {

	const Basement = 1;
	const GroundFloor = 2;
	const FirstFloor = 3;
	const SecondFloor = 4;
	const HigherThan2ndFloorNoLift = 5;
	const HigherThan2ndFloorWithLift = 6;
}
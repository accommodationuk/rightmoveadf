<?php
/**
*	This file contains the Area Units Enum class.
*
*	@package	Accommodationuk\RightmoveADF
*	@author		Jacob Wyke <jacob@frozensheep.com>
*	@license	MIT
*
*/

namespace Accommodationuk\RightmoveADF\Values;

use Accommodationuk\RightmoveADF\Values\ValuesBase;

/**
*	Area Units Enum Class
*
*	Class for the different area units.
*
*	@package	Accommodationuk\RightmoveADF
*
*/
class AreaUnits extends ValuesBase {

	const SqFt = 1;
	const SqM = 2;
	const Acre = 3;
	const Hectares = 4;
}
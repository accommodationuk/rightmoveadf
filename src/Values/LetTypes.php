<?php
/**
*	This file contains the Let Types Enum class.
*
*	@package	Accommodationuk\RightmoveADF
*	@author		Jacob Wyke <jacob@frozensheep.com>
*	@license	MIT
*
*/

namespace Accommodationuk\RightmoveADF\Values;

use Accommodationuk\RightmoveADF\Values\ValuesBase;

/**
*	Let Types Enum Class
*
*	Class for the different let types.
*
*	@package	Accommodationuk\RightmoveADF
*
*/
class LetTypes extends ValuesBase {

	const LongTerm = 1;
	const ShortTerm = 2;
	const Commercial = 4;
	const NotSpecified = 0;
}
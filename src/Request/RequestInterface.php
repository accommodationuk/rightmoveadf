<?php
/**
*	This file contains the Request Interface class.
*
*	@package	Accommodationuk\RightmoveADF
*	@author		Jacob Wyke <jacob@frozensheep.com>
*	@license	MIT
*
*/

namespace Accommodationuk\RightmoveADF\Request;

/**
*	Request Interface Class
*
*	Interface for all requests.
*
*	@package	Accommodationuk\RightmoveADF
*
*/
interface RequestInterface {

	/**
	*	Get URL Method
	*
	*	Returns the correct URL for the enviroment.
	*	@return string
	*/
	public function getURL();
}
<?php
/**
*	This file contains the Request Interface class.
*
*	@package	Accommodationuk\RightmoveADF
*	@author		Jacob Wyke <jacob@frozensheep.com>
*	@license	MIT
*
*/

namespace Accommodationuk\RightmoveADF\Request;

use Accommodationuk\RightmoveADF\Request\RequestInterface;
use Accommodationuk\RightmoveADF\RightmoveADF;
use Frozensheep\Synthesize\Synthesizer;

/**
*	Request Interface Class
*
*	Interface for all requests.
*
*	@package	Accommodationuk\RightmoveADF
*
*/
class RequestBase implements RequestInterface, \JsonSerializable {

	use Synthesizer;

	/**
	*	@var string $_strLiveURL The live request URL.
	*/
	protected $_strLiveURL = '';

	/**
	*	@var string $_strTestURL The test request URL.
	*/
	protected $_strTestURL = '';

	/**
	*	Get URL Method
	*
	*	Returns the correct URL for the environment.
	*	@param int $numEnvironment The environment we are in.
	*	@return string
	*/
	public function getURL($numEnvironment = RightmoveADF::TEST){
		return ($numEnvironment==RightmoveADF::LIVE) ? $this->_strLiveURL : $this->_strTestURL;
	}
}
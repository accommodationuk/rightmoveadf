<?php
/**
*	This file contains the Remove Featured Property Request model class.
*
*	@package	Accommodationuk\RightmoveADF
*	@author		Jacob Wyke <jacob@frozensheep.com>
*	@license	MIT
*
*/

namespace Accommodationuk\RightmoveADF\Request;

use Accommodationuk\RightmoveADF\Request\RequestBase;
use Accommodationuk\RightmoveADF\Groups\Network;
use Accommodationuk\RightmoveADF\Groups\Branch;
use Accommodationuk\RightmoveADF\Groups\PropertyRef;

/**
*	Remove Featured Property Class
*
*	Class for the remove featured property request.
*
*	@package	Accommodationuk\RightmoveADF
*
*/
class RemoveFeaturedProperty extends RequestBase {

	/**
	*	@var string $_strLiveURL The live request URL.
	*/
	protected $_strLiveURL = 'https://adfapi.rightmove.co.uk/v1/property/removefeaturedproperty';

	/**
	*	@var string $_strTestURL The test request URL.
	*/
	protected $_strTestURL = 'https://adfapi.adftest.rightmove.com/v1/property/removefeaturedproperty';

	/**
	*	@var array $arrSynthesize The synthesize array.
	*/
	protected $arrSynthesize = array(
		'network' => array('type' => 'object', 'class' => 'Accommodationuk\RightmoveADF\Groups\Network', 'required' => true),
		'branch' => array('type' => 'object', 'class' => 'Accommodationuk\RightmoveADF\Groups\Branch', 'required' => true),
		'property' => array('type' => 'object', 'class' => 'Accommodationuk\RightmoveADF\Groups\PropertyRef', 'required' => true)
	);
}
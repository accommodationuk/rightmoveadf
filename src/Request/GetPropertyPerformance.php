<?php
/**
*	This file contains the Get Property Performance Request model class.
*
*	@package	Accommodationuk\RightmoveADF
*	@author		Jacob Wyke <jacob@frozensheep.com>
*	@license	MIT
*
*/

namespace Accommodationuk\RightmoveADF\Request;

use Accommodationuk\RightmoveADF\Request\RequestBase;
use Accommodationuk\RightmoveADF\Groups\Network;
use Accommodationuk\RightmoveADF\Groups\Branch;
use Accommodationuk\RightmoveADF\Groups\PropertyRef;
use Accommodationuk\RightmoveADF\Groups\ExportPeriod;

/**
*	Get Property Property List Class
*
*	Class for get property performance request.
*
*	@package	Accommodationuk\RightmoveADF
*
*/
class GetPropertyPerformance extends RequestBase {

	/**
	*	@var string $_strLiveURL The live request URL.
	*/
	protected $_strLiveURL = 'https://adfapi.rightmove.co.uk/v1/property/getpropertyperformance';

	/**
	*	@var string $_strTestURL The test request URL.
	*/
	protected $_strTestURL = 'https://adfapi.adftest.rightmove.com/v1/property/getpropertyperformance';

	/**
	*	@var array $arrSynthesize The synthesize array.
	*/
	protected $arrSynthesize = array(
		'network' => array('type' => 'object', 'class' => 'Accommodationuk\RightmoveADF\Groups\Network', 'required' => true),
		'branch' => array('type' => 'object', 'class' => 'Accommodationuk\RightmoveADF\Groups\Branch', 'required' => true),
		'property' => array('type' => 'object', 'class' => 'Accommodationuk\RightmoveADF\Groups\PropertyRef', 'required' => true),
		'export_period' => array('type' => 'object', 'class' => 'Accommodationuk\RightmoveADF\Groups\ExportPeriod', 'required' => true)
	);
}
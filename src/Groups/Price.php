<?php
/**
*	This file contains the Price Group model class.
*
*	@package	Accommodationuk\RightmoveADF
*	@author		Jacob Wyke <jacob@frozensheep.com>
*	@license	MIT
*
*/

namespace Accommodationuk\RightmoveADF\Groups;

use Accommodationuk\RightmoveADF\Groups\GroupInterface;
use Frozensheep\Synthesize\Synthesizer;
use Accommodationuk\RightmoveADF\Values\PriceQualifiers;
use Accommodationuk\RightmoveADF\Values\RentFrequencies;
use Accommodationuk\RightmoveADF\Values\TenureTypes;

/**
*	Price Group Class
*
*	Class to handle Price group.
*
*	@package	Accommodationuk\RightmoveADF
*
*/
class Price implements GroupInterface, \JsonSerializable {

	use Synthesizer;

	protected $arrSynthesize = array(
		'price' => array('type' => 'number', 'required' => true, 'min' => 1),
		'price_qualifier' => array('type' => 'enum', 'class' => 'Accommodationuk\RightmoveADF\Values\PriceQualifiers'),
		'deposit' => array('type' => 'number'),
		'administration_fee' => array('type' => 'string', 'max' => 4000),
		'rent_frequency' => array('type' => 'enum', 'class' => 'Accommodationuk\RightmoveADF\Values\RentFrequencies'),
		'tenure_type' => array('type' => 'enum', 'class' => 'Accommodationuk\RightmoveADF\Values\TenureTypes'),
		'auction' => array('type' => 'boolean'),
		'tenure_unexpired_years' => array('type' => 'int'),
		'price_per_unit_area' => array('type' => 'number'),
		'price_per_unit_per_annum' => array('type' => 'number')
	);
}
<?php
/**
*	This file contains the Network Group model class.
*
*	@package	Accommodationuk\RightmoveADF
*	@author		Jacob Wyke <jacob@frozensheep.com>
*	@license	MIT
*
*/

namespace Accommodationuk\RightmoveADF\Groups;

use Accommodationuk\RightmoveADF\Groups\GroupInterface;
use Frozensheep\Synthesize\Synthesizer;

/**
*	Network Group Class
*
*	Class to handle Network group.
*
*	@package	Accommodationuk\RightmoveADF
*
*/
class Network implements GroupInterface, \JsonSerializable {

	use Synthesizer;

	protected $arrSynthesize = array(
		'network_id' => array('type' => 'int', 'required' => true)
	);
}
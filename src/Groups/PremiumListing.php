<?php
/**
*	This file contains the Premium Listing Group model class.
*
*	@package	Accommodationuk\RightmoveADF
*	@author		Jacob Wyke <jacob@frozensheep.com>
*	@license	MIT
*
*/

namespace Accommodationuk\RightmoveADF\Groups;

use Accommodationuk\RightmoveADF\Groups\GroupInterface;
use Frozensheep\Synthesize\Synthesizer;
use Accommodationuk\RightmoveADF\Values\DisplayTypes;
use Accommodationuk\RightmoveADF\Values\StampTexts;

/**
*	Premium Listing  Group Class
*
*	Class to handle Premium Listing group.
*
*	@package	Accommodationuk\RightmoveADF
*
*/
class PremiumListing implements GroupInterface, \JsonSerializable {

	use Synthesizer;

	protected $arrSynthesize = array(
		'display_type' => array('type' => 'enum', 'class' => 'Accommodationuk\RightmoveADF\Values\DisplayTypes', 'required' => true),
		'resale_stamp_text' => array('type' => 'enum', 'class' => 'Accommodationuk\RightmoveADF\Values\StampTexts'),
		'lettings_stamp_text' => array('type' => 'enum', 'class' => 'Accommodationuk\RightmoveADF\Values\StampTexts'),
		'web_flag' => array('type' => 'boolean', 'default' => false, 'required' => true),
		'mobile_flag' => array('type' => 'boolean', 'default' => false, 'required' => true)
	);
}
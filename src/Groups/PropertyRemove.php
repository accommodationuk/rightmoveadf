<?php
/**
*	This file contains the Property Remove Group model class.
*
*	@package	Accommodationuk\RightmoveADF
*	@author		Jacob Wyke <jacob@frozensheep.com>
*	@license	MIT
*
*/

namespace Accommodationuk\RightmoveADF\Groups;

use Accommodationuk\RightmoveADF\Groups\GroupInterface;
use Frozensheep\Synthesize\Synthesizer;
use Accommodationuk\RightmoveADF\Values\RemovalReasons;

/**
*	Property Remove Group Class
*
*	Class to handle Property group.
*
*	@package	Accommodationuk\RightmoveADF
*
*/
class PropertyRemove implements GroupInterface, \JsonSerializable {

	use Synthesizer;

	protected $arrSynthesize = array(
		'agent_ref' => array('type' => 'string', 'required' => true, 'max' => 80),
		'removal_reason' => array('type' => 'enum', 'class' => 'Accommodationuk\RightmoveADF\Values\RemovalReasons'),
		'transaction_date' => array('type' => 'datetime', 'format' => 'd-m-Y G:i:s', 'autoinit' => false)
	);
}
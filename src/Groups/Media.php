<?php
/**
*	This file contains the Media Group model class.
*
*	@package	Accommodationuk\RightmoveADF
*	@author		Jacob Wyke <jacob@frozensheep.com>
*	@license	MIT
*
*/

namespace Accommodationuk\RightmoveADF\Groups;

use Accommodationuk\RightmoveADF\Groups\GroupInterface;
use Frozensheep\Synthesize\Synthesizer;
use Accommodationuk\RightmoveADF\Values\MediaTypes;

/**
*	Media Group Class
*
*	Class to handle Media group.
*
*	@package	Accommodationuk\RightmoveADF
*
*/
class Media implements GroupInterface, \JsonSerializable {

	use Synthesizer;

	protected $arrSynthesize = array(
		'media_type' => array('type' => 'enum', 'class' => 'Accommodationuk\RightmoveADF\Values\MediaTypes', 'required' => true),
		'media_url' => array('type' => 'string', 'required' => true, 'max' => 250),
		'caption' => array('type' => 'string', 'max' => 50),
		'sort_order' => array('type' => 'int', 'min' => 0),
		'media_update_date' => array('type' => 'datetime', 'format' => 'd-m-Y G:i:s', 'autoinit' => false)
	);
}
<?php
/**
*	This file contains the Property Feature Group model class.
*
*	@package	Accommodationuk\RightmoveADF
*	@author		Jacob Wyke <jacob@frozensheep.com>
*	@license	MIT
*
*/

namespace Accommodationuk\RightmoveADF\Groups;

use Accommodationuk\RightmoveADF\Groups\GroupInterface;
use Frozensheep\Synthesize\Synthesizer;
use Accommodationuk\RightmoveADF\Groups\FeaturedProperty;

/**
*	Property Feature Group Class
*
*	Class to handle Property group.
*
*	@package	Accommodationuk\RightmoveADF
*
*/
class PropertyFeature implements GroupInterface, \JsonSerializable {

	use Synthesizer;

	protected $arrSynthesize = array(
		'agent_ref' => array('type' => 'string', 'required' => true, 'max' => 80),
		'featured_property' => array('type' => 'object', 'class' => 'Accommodationuk\RightmoveADF\Groups\FeaturedProperty', 'required' => true)
	);
}
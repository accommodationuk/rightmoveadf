<?php
/**
*	This file contains the Details Group model class.
*
*	@package	Accommodationuk\RightmoveADF
*	@author		Jacob Wyke <jacob@frozensheep.com>
*	@license	MIT
*
*/

namespace Accommodationuk\RightmoveADF\Groups;

use Accommodationuk\RightmoveADF\Groups\GroupInterface;
use Frozensheep\Synthesize\Synthesizer;
use Accommodationuk\RightmoveADF\Values\Parkings;
use Accommodationuk\RightmoveADF\Values\OutsideSpaces;
use Accommodationuk\RightmoveADF\Values\AreaUnits;
use Accommodationuk\RightmoveADF\Values\EntranceFloors;
use Accommodationuk\RightmoveADF\Values\Conditions;
use Accommodationuk\RightmoveADF\Values\Accessibilities;
use Accommodationuk\RightmoveADF\Values\Heatings;
use Accommodationuk\RightmoveADF\Values\Furnishings;
use Accommodationuk\RightmoveADF\Values\CommercialuseClasses;
use Accommodationuk\RightmoveADF\Groups\Room;

/**
*	Details Group Class
*
*	Class to handle Details group.
*
*	@package	Accommodationuk\RightmoveADF
*
*/
class Details implements GroupInterface, \JsonSerializable {

	use Synthesizer;

	protected $arrSynthesize = array(
		'summary' => array('type' => 'string', 'required' => true, 'max' => 1000),
		'description' => array('type' => 'string', 'required' => true, 'max' => 32000),
		'features' => array('type' => 'objectarray', 'class' => 'Frozensheep\Synthesize\Type\StringObject', 'max' => 10),
		'bedrooms' => array('type' => 'int', 'required' => true, 'min' => 0),
		'bathrooms' => array('type' => 'int', 'min' => 0),
		'reception_rooms' => array('type' => 'int', 'min' => 0),
		'parking' => array('type' => 'objectarray', 'class' => 'Accommodationuk\RightmoveADF\Values\Parkings'),
		'outside_space' => array('type' => 'objectarray', 'class' => 'Accommodationuk\RightmoveADF\Values\OutsideSpaces'),
		'year_built' => array('type' => 'int'),
		'internal_area' => array('type' => 'number'),
		'internal_area_unit' => array('type' => 'enum', 'class' => 'Accommodationuk\RightmoveADF\Values\AreaUnits'),
		'land_area' => array('type' => 'number'),
		'land_area_unit' => array('type' => 'enum', 'class' => 'Accommodationuk\RightmoveADF\Values\AreaUnits'),
		'minimum' => array('type' => 'int', 'min' => 0),
		'maximum' => array('type' => 'int', 'min' => 0),
		'area_unit' => array('type' => 'enum', 'class' => 'Accommodationuk\RightmoveADF\Values\AreaUnits'),
		'floors' => array('type' => 'int'),
		'entrance_floor' => array('type' => 'enum', 'class' => 'Accommodationuk\RightmoveADF\Values\EntranceFloors'),
		'condition' => array('type' => 'enum', 'class' => 'Accommodationuk\RightmoveADF\Values\Conditions'),
		'accessibility' => array('type' => 'objectarray', 'class' => 'Accommodationuk\RightmoveADF\Values\Accessibilites'),
		'heating' => array('type' => 'objectarray', 'class' => 'Accommodationuk\RightmoveADF\Values\Heatings'),
		'furnished_type' => array('type' => 'enum', 'class' => 'Accommodationuk\RightmoveADF\Values\Furnishings'),
		'pets_allowed' => array('type' => 'boolean'),
		'smokers_considered' => array('type' => 'boolean'),
		'housing_benefit_considered' => array('type' => 'boolean'),
		'sharers_considered' => array('type' => 'boolean'),
		'burglar_alarm' => array('type' => 'boolean'),
		'washing_machine' => array('type' => 'boolean'),
		'dishwasher' => array('type' => 'boolean'),
		'all_bills_inc' => array('type' => 'boolean'),
		'water_bill_inc' => array('type' => 'boolean'),
		'gas_bill_inc' => array('type' => 'boolean'),
		'electricity_bill_inc' => array('type' => 'boolean'),
		'oil_bill_inc' => array('type' => 'boolean'),
		'council_tax_inc' => array('type' => 'boolean'),
		'tv_licence_inc' => array('type' => 'boolean'),
		'sat_cable_tv_bill_inc' => array('type' => 'boolean'),
		'internet_bill_inc' => array('type' => 'boolean'),
		'business_for_sale' => array('type' => 'boolean'),
		'comm_use_class' => array('type' => 'objectarray', 'class' => 'Accommodationuk\RightmoveADF\Values\CommercialuseClasses'),
		'rooms' => array('type' => 'objectarray', 'class' => 'Accommodationuk\RightmoveADF\Groups\Room', 'max' => 99)
	);
}
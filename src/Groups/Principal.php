<?php
/**
*	This file contains the Principal Group model class.
*
*	@package	Accommodationuk\RightmoveADF
*	@author		Jacob Wyke <jacob@frozensheep.com>
*	@license	MIT
*
*/

namespace Accommodationuk\RightmoveADF\Groups;

use Accommodationuk\RightmoveADF\Groups\GroupInterface;
use Frozensheep\Synthesize\Synthesizer;

/**
*	Principal Group Class
*
*	Class to handle Principal group.
*
*	@package	Accommodationuk\RightmoveADF
*
*/
class Principal implements GroupInterface, \JsonSerializable {

	use Synthesizer;

	protected $arrSynthesize = array(
		'principal_email_address' => array('type' => 'string', 'required' => true, 'max' => 200),
		'auto_email_when_live' => array('type' => 'boolean'),
		'auto_email_updates' => array('type' => 'boolean')
	);
}
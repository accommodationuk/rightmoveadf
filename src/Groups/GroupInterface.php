<?php
/**
*	This file contains the Group Interface class.
*
*	@package	Accommodationuk\RightmoveADF
*	@author		Jacob Wyke <jacob@frozensheep.com>
*	@license	MIT
*
*/

namespace Accommodationuk\RightmoveADF\Groups;

/**
*	Group Interface Class
*
*	Interface for all of the Groups.
*
*	@package	Accommodationuk\RightmoveADF
*
*/
interface GroupInterface {

}